/*
 * Copyright (C) 2016 Southern Storm Software, Pty Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package com.southernstorm.noise.protocol;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.ShortBufferException;

import com.southernstorm.noise.crypto.chacha20.ChaChaCore;
import com.southernstorm.noise.crypto.Poly1305;

import io.github.garyttierney.chacha20poly1305.AeadCipher;
import io.github.garyttierney.chacha20poly1305.AeadCipherException;
import io.github.garyttierney.chacha20poly1305.AeadVerificationException;
import io.github.garyttierney.chacha20poly1305.AeadMode;
import io.github.garyttierney.chacha20poly1305.common.SecretKey;

/**
 * Implements the ChaChaPoly cipher for Noise.
 */
public class ChaChaPolyCipherState implements CipherState {

	private final Poly1305 poly;
	private final int[] input;
	private final int[] output;
	private final byte[] polyKey;
	private long n;
	private boolean haskey;
	// Debug only
	private byte[] initialKey;

	private static final boolean DEBUG = false;

	private static final boolean USESODIUM = false;
	private SecretKey _sodiumKey;
	private ByteBuffer _sodiumNonce;
	private ByteBuffer _sodiumAd;
	private AeadCipher _sodium;
	private static ByteBuffer NULL_BUFFER;

	static {
		boolean bin = false;
		try {
			System.loadLibrary("chacha20_poly1305_aead");
			bin = true;
			NULL_BUFFER = ByteBuffer.allocate(0);
			System.out.println("Using libsodium");
		} catch (UnsatisfiedLinkError ule) {
		}
		// USESODIUM = bin;
	}
	
	/**
	 * Constructs a new cipher state for the "ChaChaPoly" algorithm.
	 */
	public ChaChaPolyCipherState()
	{
		poly = new Poly1305();
		input = new int [16];
		output = new int [16];
		polyKey = new byte [32];
		n = 0;
		haskey = false;
		if (USESODIUM) {
			_sodiumNonce = ByteBuffer.allocateDirect(12);
			_sodiumNonce.order(ByteOrder.LITTLE_ENDIAN);
		}
	}

	/**
	 * Copy constructor for cloning
	 * @since 0.9.44
	 */
	protected ChaChaPolyCipherState(ChaChaPolyCipherState o) throws CloneNotSupportedException {
		poly = o.poly.clone();
		input = Arrays.copyOf(o.input, o.input.length);
		output = Arrays.copyOf(o.output, o.output.length);
		polyKey = Arrays.copyOf(o.polyKey, o.polyKey.length);
		n = o.n;
		haskey = o.haskey;
		initialKey = o.initialKey;
		if (USESODIUM) {
			_sodiumNonce = ByteBuffer.allocateDirect(12); // filled by setup()
			_sodiumNonce.order(ByteOrder.LITTLE_ENDIAN);
			if (haskey) {
				byte[] key = new byte[32];
				o._sodiumKey.getKey().get(key);
				_sodiumKey = new SecretKey(key);
			}
		}
	}

	@Override
	public void destroy() {
		poly.destroy();
		Arrays.fill(input, 0);
		Arrays.fill(output, 0);
		Noise.destroy(polyKey);
	}

	@Override
	public String getCipherName() {
		return "ChaChaPoly";
	}

	@Override
	public int getKeyLength() {
		return 32;
	}

	@Override
	public int getMACLength() {
		return haskey ? 16 : 0;
	}

	@Override
	public void initializeKey(byte[] key, int offset) {
		initialKey = Arrays.copyOfRange(key, offset, offset + 32);
		if (USESODIUM) {
			_sodiumKey = new SecretKey(initialKey);
			try {
				_sodium = new AeadCipher(_sodiumKey, AeadMode.CHACHA20_POLY1305_IETF);
			} catch (AeadCipherException e) {
				System.out.println("sodium key failed " + e);
			}
		} else {
			ChaChaCore.initKey256(input, key, offset);
		}
		n = 0;
		haskey = true;
	}

	@Override
	public boolean hasKey() {
		return haskey;
	}
	
	/**
	 * Set up to encrypt or decrypt the next packet.
	 * 
	 * @param ad The associated data for the packet.
	 */
	private void setup(byte[] ad) {
		if (n == -1L)
			throw new IllegalStateException("Nonce has wrapped around");
		if (USESODIUM) {
			if (ad != null) {
				_sodiumAd = ByteBuffer.allocateDirect(ad.length);
				_sodiumAd.put(ad);
				_sodiumAd.rewind();
			} else
				_sodiumAd = NULL_BUFFER;
			_sodiumNonce.putLong(4, n++);
			return;
		}
		ChaChaCore.initIV(input, n++);
		ChaChaCore.hash(output, input);
		int index = 0;
		for (int posn = 0; posn < 8; posn++) {
			int value = output[posn];
			polyKey[index++] = (byte)(value);
			polyKey[index++] = (byte)(value >> 8);
			polyKey[index++] = (byte)(value >> 16);
			polyKey[index++] = (byte)(value >> 24);
		}
		poly.reset(polyKey, 0);
		if (ad != null) {
			poly.update(ad, 0, ad.length);
			poly.pad();
		}
		// if (++(input[12]) == 0)
		// 	++(input[13]);
	}

	/**
	 * Puts a 64-bit integer into a buffer in little-endian order.
	 * 
	 * @param output The output buffer.
	 * @param offset The offset into the output buffer.
	 * @param value The 64-bit integer value.
	 */
	private static void putLittleEndian64(byte[] output, int offset, long value)
	{
		output[offset] = (byte)value;
		output[offset + 1] = (byte)(value >> 8);
		output[offset + 2] = (byte)(value >> 16);
		output[offset + 3] = (byte)(value >> 24);
		output[offset + 4] = (byte)(value >> 32);
		output[offset + 5] = (byte)(value >> 40);
		output[offset + 6] = (byte)(value >> 48);
		output[offset + 7] = (byte)(value >> 56);
	}

	/**
	 * Finishes up the authentication tag for a packet.
	 * 
	 * @param ad The associated data.
	 * @param length The length of the plaintext data.
	 */
	private void finish(byte[] ad, int length)
	{
		poly.pad();
		putLittleEndian64(polyKey, 0, ad != null ? ad.length : 0);
		putLittleEndian64(polyKey, 8, length);
		poly.update(polyKey, 0, 16);
		poly.finish(polyKey, 0);
	}

	/**
	 * Encrypts or decrypts a buffer of bytes for the active packet.
	 * 
	 * @param plaintext The plaintext data to be encrypted.
	 * @param plaintextOffset The offset to the first plaintext byte.
	 * @param ciphertext The ciphertext data that results from encryption.
	 * @param ciphertextOffset The offset to the first ciphertext byte.
	 * @param length The number of bytes to encrypt.
	 */
	private void encrypt(byte[] plaintext, int plaintextOffset,
			byte[] ciphertext, int ciphertextOffset, int length) {
		int rounds = (length - 1) >> 6;
		length &= 0x3f;
		int len1 = 16;
        for (int i = rounds; i >= 0; i--) {
			++input[12];
			ChaChaCore.hash(output, input);
            if (i == 0 && length > 0) {
				if ((length & 3) > 0) {
					ChaChaCore.xorBlock(plaintext, plaintextOffset, ciphertext, ciphertextOffset, length, output);
					return;
				}
                len1 = length >> 2;
            }
			int posn;
			int value;
			for (posn = 0; posn < len1; posn++) {
				value = output[posn];
				ciphertext[ciphertextOffset++] = (byte)(plaintext[plaintextOffset++] ^ value);
				ciphertext[ciphertextOffset++] = (byte)(plaintext[plaintextOffset++] ^ value >> 8);
				ciphertext[ciphertextOffset++] = (byte)(plaintext[plaintextOffset++] ^ value >> 16);
				ciphertext[ciphertextOffset++] = (byte)(plaintext[plaintextOffset++] ^ value >> 24);
			}
		// if (++(input[12]) == 0)
		// 	++(input[13]);
		}
	}

	@Override
	public int encryptWithAd(byte[] ad, byte[] plaintext, int plaintextOffset,
			byte[] ciphertext, int ciphertextOffset, int length) throws ShortBufferException {
		int space;
		if (ciphertextOffset > ciphertext.length)
			space = 0;
		else
			space = ciphertext.length - ciphertextOffset;
		if (!haskey) {
			// The key is not set yet - return the plaintext as-is.
			if (length > space)
				throw new ShortBufferException();
			if (plaintext != ciphertext || plaintextOffset != ciphertextOffset)
				System.arraycopy(plaintext, plaintextOffset, ciphertext, ciphertextOffset, length);
			return length;
		}
		if (space < 16 || length > (space - 16))
			throw new ShortBufferException();
		setup(ad);
		if (USESODIUM) {
			try {
				ByteBuffer temp = ByteBuffer.allocateDirect(length);
				temp.put(plaintext, plaintextOffset, length);
				temp.rewind();
				ByteBuffer rv = _sodium.encrypt(temp, length, ad == null ? null : _sodiumAd, ad == null ? 0 : ad.length, _sodiumNonce);
				rv.rewind();
				rv.get(ciphertext, ciphertextOffset, length + 16);
			} catch (AeadCipherException e) {
				System.out.println("sodium encrypt failed " + e);
			}
		} else {
			encrypt(plaintext, plaintextOffset, ciphertext, ciphertextOffset, length);
			poly.update(ciphertext, ciphertextOffset, length);
			finish(ad, length);
			System.arraycopy(polyKey, 0, ciphertext, ciphertextOffset + length, 16);
		}
		return length + 16;
	}

	@Override
	public int decryptWithAd(byte[] ad, byte[] ciphertext,
			int ciphertextOffset, byte[] plaintext, int plaintextOffset,
			int length) throws ShortBufferException, BadPaddingException {
		int space;
		if (ciphertextOffset > ciphertext.length)
			space = 0;
		else
			space = ciphertext.length - ciphertextOffset;
		if (length > space)
			throw new ShortBufferException();
		if (plaintextOffset > plaintext.length)
			space = 0;
		else
			space = plaintext.length - plaintextOffset;
		if (!haskey) {
			// The key is not set yet - return the ciphertext as-is.
			if (length > space)
				throw new ShortBufferException();
			if (plaintext != ciphertext || plaintextOffset != ciphertextOffset)
				System.arraycopy(ciphertext, ciphertextOffset, plaintext, plaintextOffset, length);
			return length;
		}
		if (length < 16)
			Noise.throwBadTagException();
		int dataLen = length - 16;
		if (dataLen > space)
			throw new ShortBufferException();
		setup(ad);
		if (USESODIUM) {
			try {
				ByteBuffer temp = ByteBuffer.allocateDirect(length);
				temp.put(ciphertext, ciphertextOffset, length);
				temp.rewind();
				ByteBuffer rv = _sodium.decrypt(temp,
						length, ad == null ? null : _sodiumAd, ad == null ? 0 : ad.length, _sodiumNonce);
				rv.rewind();
				rv.get(plaintext, plaintextOffset, dataLen);
			} catch (AeadCipherException e) {
				System.out.println("sodium decrypt failed " + e);
			} catch (AeadVerificationException e) {
				Noise.throwBadTagException();
			}
		} else {
			poly.update(ciphertext, ciphertextOffset, dataLen);
			finish(ad, dataLen);
			int temp = 0;
			for (int index = 0; index < 16; ++index)
				temp |= (polyKey[index] ^ ciphertext[ciphertextOffset + dataLen + index]);
			if ((temp & 0xFF) != 0)
				Noise.throwBadTagException();
			encrypt(ciphertext, ciphertextOffset, plaintext, plaintextOffset, dataLen);
		}
		return dataLen;
	}

	@Override
	public CipherState fork(byte[] key, int offset) {
		CipherState cipher = new ChaChaPolyCipherState();
		cipher.initializeKey(key, offset);
		return cipher;
	}

	@Override
	public void setNonce(long nonce) {
		n = nonce;
	}

	/**
	 *  I2P
	 *  @since 0.9.44
	 */
	@Override
	public ChaChaPolyCipherState clone() throws CloneNotSupportedException {
		return new ChaChaPolyCipherState(this);
	}

	/**
	 *  I2P debug
	 */
	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("  Cipher State:\n" +
		           "    nonce: ");
		buf.append(n);
		// I2P debug
		if (DEBUG) {
			buf.append("\n" +
			           "    init key: ");
			if (haskey)
				buf.append(net.i2p.data.Base64.encode(initialKey));
			else
				buf.append("null");
		}
		buf.append("\n    poly key: ");
		if (haskey)
			buf.append(net.i2p.data.Base64.encode(polyKey));
		else
			buf.append("null");
		buf.append('\n');
		return buf.toString();
	}
}
