package net.i2p.router;

import net.i2p.util.I2PThread;
import net.i2p.util.Log;
import net.i2p.util.SystemVersion;

/** a do run run run a do run run */
class JobQueueRunner extends I2PThread {
    private final Log _log;
    private final RouterContext _context;
    private final int _id;
    private Job _currentJob;
    private Job _lastJob;
    private long _lastBegin;
    private long _lastEnd;
    
    public JobQueueRunner(RouterContext context, int id) {
        _context = context;
        _id = id;
        _log = _context.logManager().getLog(JobQueueRunner.class);
        setPriority(NORM_PRIORITY + 1);
        // all createRateStat in JobQueue
    }
    
    public Job getCurrentJob() { return _currentJob; }
    public Job getLastJob() { return _lastJob; }
    public int getRunnerId() { return _id; }
    public long getLastBegin() { return _lastBegin; }
    public long getLastEnd() { return _lastEnd; }

    public void run() {
        while (_context.jobQueue().isAlive()) { 
            try {
                Job job = _context.jobQueue().getNext(_id);
                if (job == null) {
                    if (_context.router().isAlive())
                        if (_log.shouldLog(Log.INFO))
                            _log.error("getNext returned null - dead?");
                    continue;
                }
                long now = _context.clock().now();

                _currentJob = job;
                _lastJob = null;
                if (_log.shouldLog(Log.DEBUG))
                    _log.debug(this.getName() + " running job " + job.getJobId() + ": " + job.getName());
                long lag = now - job.getStartAfter();
                if (lag < 0) lag = 0;
                job.start(now);
                runCurrentJob();
                now = _context.clock().now();
                job.end(now);

                long duration = now - job.getActualStart();
                if (duration > 1000) {
                    _context.statManager().addRateData("jobQueue.jobRunSlow", duration, duration);
                    if (_log.shouldLog(Log.WARN))
                        _log.warn("Duration of " + duration + " (lag "+ lag + ") on job " + _currentJob);
                }
                
                if (_log.shouldLog(Log.DEBUG))
                    _log.debug("Job duration " + duration + "ms for " + job.getName() + " with lag of " + lag + "ms");
                _lastEnd = now;
                _lastJob = _currentJob;
                _currentJob = null;
            } catch (Throwable t) {
                _log.log(Log.CRIT, "error running?", t);
            }
        }
        if (_context.router().isAlive() && _log.shouldLog(Log.INFO))
            _log.log(Log.INFO, "Queue runner " + this.getName() + " exiting");
    }
    
    private void runCurrentJob() {
        try {
            _lastBegin = _context.clock().now();
            _currentJob.runJob();
        } catch (OutOfMemoryError oom) {
            try {
                if (SystemVersion.isAndroid())
                    _context.router().shutdown(Router.EXIT_OOM);
                else
                    fireOOM(oom);
            } catch (Throwable t) {}
        } catch (Throwable t) {
            _log.log(Log.CRIT, "Error processing job [" + _currentJob.getName() 
                                   + "] on thread " + _id + ": " + t, t);
        }
    }
}
