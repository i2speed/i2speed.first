package net.i2p.router.tunnel;

import net.i2p.router.util.CachedIteratorCollection;
import net.i2p.router.RouterContext;
import net.i2p.router.Job;
import net.i2p.router.JobImpl;

/**
 * straight pumping for inbound and outbound I2CP receivers
**/

class TunnelGatewayPumper {
    private final RouterContext _context;
    private final CachedIteratorCollection<PumpedTunnelGateway> _backlogged;
    private final CachedIteratorCollection<PumpedTunnelGateway> _livepumps;
    private final CachedIteratorCollection<PumpedTunnelGateway> _wantsPumping;
    private volatile boolean _stop;

    /**
     *  Wait just a little, but this lets the pumper queue back up.
     *  See additional comments in PTG.
     */
    private static final long REQUEUE_TIME = 50;

    /** Creates a new instance of TunnelGatewayPumper */
    public TunnelGatewayPumper(RouterContext ctx) {
        _context = ctx;
        _backlogged = new CachedIteratorCollection<PumpedTunnelGateway>();
        _livepumps = new CachedIteratorCollection<PumpedTunnelGateway>();
        _wantsPumping = new CachedIteratorCollection<PumpedTunnelGateway>();
    }

    public void stopPumping() {
        _stop = true;
        synchronized (_wantsPumping) {
            _backlogged.clear();
            _livepumps.clear();
            _wantsPumping.clear();
        }
    }
    
    public void wantsPumping(PumpedTunnelGateway gw) {
        synchronized (_wantsPumping) {
            if (_wantsPumping.contains(gw) || _backlogged.contains(gw)) return;
            // let others return early if somebody else working already
            if (_livepumps.contains(gw)) {
                _wantsPumping.add(gw);
                return;
            }
            _livepumps.add(gw);
        }
        while (!_stop) {
            if (gw.pump()) {  // once per minute with the improved transport backlogging
                synchronized (_wantsPumping) {
                    _backlogged.add(gw);
                    requeue(gw);
                    _wantsPumping.remove(gw);
                    _livepumps.remove(gw);
                }
                return;
            }
            if (gw.hasPreQueue()) continue; // avoid excessive locking
            synchronized (_wantsPumping) {
                if (!_wantsPumping.remove(gw) || !gw.hasPreQueue()) {
                    _livepumps.remove(gw);
                    break;
                }
            }
        }
    }

    private void requeue(PumpedTunnelGateway ptg) {

        final PumpedTunnelGateway gw = ptg;

        final class RequeueJob extends JobImpl {

            private RequeueJob() {
                super(_context);
            }

            public void runJob() {
                synchronized (_wantsPumping) {
                    _backlogged.remove(gw);
                }
                wantsPumping(gw);
            }
            public String getName() { return "Requeued tunnel gateway message"; }
        }

        Job j = new RequeueJob();
        j.setStartAfter(_context.clock().now() + REQUEUE_TIME);
        _context.jobQueue().addJob(j);
    }
}
