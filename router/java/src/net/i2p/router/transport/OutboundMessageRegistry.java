package net.i2p.router.transport;
/*
 * free (adj.): unencumbered; not under the control of others
 * Written by jrandom in 2003 and released into the public domain 
 * with no warranty of any kind, either expressed or implied.  
 * It probably won't make your computer catch on fire, or eat 
 * your children, but it might.  Use at your own risk.
 *
 */

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.i2p.data.i2np.I2NPMessage;
import net.i2p.router.Job;
import net.i2p.router.MessageSelector;
import net.i2p.router.OutNetMessage;
import net.i2p.router.ReplyJob;
import net.i2p.router.RouterContext;
import net.i2p.util.Log;
import net.i2p.data.i2np.DeliveryStatusMessage;
import net.i2p.data.i2np.DatabaseStoreMessage;
import net.i2p.data.i2np.DatabaseSearchReplyMessage;

/**
 *  Tracks outbound messages.
 */
public class OutboundMessageRegistry {
    private final Log _log;
    /** set of currently active MessageSelector instances */
    private final ConcurrentLinkedQueue<MessageSelector> _selectors;
    /** map of active MessageSelector to either an OutNetMessage or a List of OutNetMessages causing it (for quick removal) */
    private final Map<MessageSelector, Object> _selectorToMessage;
    /**
     *  set of active OutNetMessage (for quick removal and selector fetching)
     *  !! Really? seems only for dup detection in registerPending().
     *  Changed to concurrent, but it could perhaps be removed completely,
     *  It would seem difficult to add a dup since every OutNetMessage is different,
     *  and it's generally instantiated just before ctx.outNetMessagePool().add().
     *  But in TransportImpl.afterSend() it does requeue a previous ONM if allowRequeue=true.
     */
    private final RouterContext _context;
    
    private long _nextExpire;
    public OutboundMessageRegistry(RouterContext context) {
        _context = context;
        _log = _context.logManager().getLog(OutboundMessageRegistry.class);
        _selectors = new ConcurrentLinkedQueue<MessageSelector>();
        _selectorToMessage = new HashMap<MessageSelector, Object>(64);
        _nextExpire = _context.clock().now() + 10 * 1000;
    }
    
    /**
     *  Does something @since 0.8.8
     */
    public void shutdown() {
            _selectors.clear();
        synchronized (_selectorToMessage) { 
            _selectorToMessage.clear();
        }
        // Calling the fail job for every active message would
        // be way too much at shutdown/restart, right?
    }
    
    /**
     *  @since 0.8.8
     */
    public void restart() {
        shutdown();
    }
    
    /**
     * Retrieve all messages that are waiting for the specified message.  In
     * addition, those matches may include instructions to either continue or not
     * continue waiting for further replies - if it should continue, the matched
     * message remains in the registry, but if it shouldn't continue, the matched
     * message is removed from the registry.  
     *
     * This is called only by InNetMessagePool.
     *
     * TODO this calls isMatch() in the selectors from inside the lock, which
     * can lead to deadlocks if the selector does too much in isMatch().
     * Remove the lock if possible.
     *
     * @param message Payload received that may be a reply to something we sent
     * @return non-null List of OutNetMessage describing messages that were waiting for 
     *         the payload
     */
    @SuppressWarnings("unchecked")
    public List<OutNetMessage> getOriginalMessages(I2NPMessage message) {
        boolean shortcut = message instanceof DeliveryStatusMessage;
        if (!(shortcut || message instanceof DatabaseStoreMessage || message instanceof DatabaseSearchReplyMessage))
            return null;
        List<MessageSelector> matchedSelectors = null;
        List<MessageSelector> removedSelectors = null;
        boolean expiring = false;
        long now = 0;
        boolean log = false;
        MessageSelector sel = null;
        List<OutNetMessage> rv = null;

        if (!shortcut) {
            now = _context.clock().now();
            expiring = _nextExpire <= now;
            if (expiring) {
                _nextExpire = now + 10 *1000;
                log = _log.shouldDebug();
            }
        }
        for (Iterator<MessageSelector> iter = _selectors.iterator(); iter.hasNext(); ) {
            sel = iter.next();
            if (expiring) {
                long expiration = sel.getExpiration();
                if (expiration <= now) {
                    iter.remove();
                    OutNetMessage msg = null;
                    List<OutNetMessage> msgs = null;
                    Object o;
                    synchronized (_selectorToMessage) {
                        o = _selectorToMessage.remove(sel);
                    }
                    if (o instanceof OutNetMessage) {
                        msg = (OutNetMessage)o;
                    } else if (o instanceof List) {
                        msgs = (List<OutNetMessage>)o;
                    }
                    if (msg != null) {
                        Job fail = msg.getOnFailedReplyJob();
                        if (fail != null)
                            _context.jobQueue().addJob(fail);
                        if (log)
                            _log.debug("Expired: " + sel + " with timeout job " + fail);
                    } else if (msgs != null) {
                        for (OutNetMessage m : msgs) {
                            Job fail = m.getOnFailedReplyJob();
                            if (fail != null)
                                _context.jobQueue().addJob(fail);
                            if (log)
                                _log.debug("Expired: " + sel + " with timeout job(s) " + fail);
                        }
                    } else {
                        if (log)
                            _log.debug("Expired: " + sel + " with no known messages");
                    }
                    if (log) {
                        int r = _selectors.size();
                        if (r > 0)
                            _log.debug("Expired: " + 1 + " remaining: " + r);
                    }
                    continue;
                }
                _nextExpire = Math.min(_nextExpire, expiration);
            }
            if (sel.isMatch(message)) {
                if (shortcut) {
                    iter.remove();
                    rv = new ArrayList<OutNetMessage>(1);
                    OutNetMessage msg = null;
                    List<OutNetMessage> msgs = null;
                    Object o;
                    synchronized (_selectorToMessage) {
                        o = _selectorToMessage.remove(sel);
                    }
                    if (o instanceof OutNetMessage) {
                        msg = (OutNetMessage)o;
                        rv.add(msg);
                    } else if (o instanceof List) {
                        msgs = (List<OutNetMessage>)o;
                        rv.addAll(msgs);
                    }
                    return rv;
                };
                if (sel.continueMatching()) {
                    if (matchedSelectors == null) matchedSelectors = new ArrayList<MessageSelector>(1);
                    matchedSelectors.add(sel);
                } else {
                    iter.remove();
                        if (removedSelectors == null) removedSelectors = new ArrayList<MessageSelector>(1);
                        removedSelectors.add(sel);
                }
            }  
        }

        if (matchedSelectors != null) {
            rv = new ArrayList<OutNetMessage>(matchedSelectors.size());
            synchronized (_selectorToMessage) {
                for (MessageSelector sel1 : matchedSelectors) {
                OutNetMessage msg = null;
                List<OutNetMessage> msgs = null;
                    Object o = _selectorToMessage.get(sel1);
                    
                    if (o instanceof OutNetMessage) {
                        msg = (OutNetMessage)o;
                        rv.add(msg);
                    } else if (o instanceof List) {
                        msgs = (List<OutNetMessage>)o;
                        rv.addAll(msgs);
                    }
                }
                    }
                }
        if (removedSelectors != null) {
            if (rv == null) rv = new ArrayList<OutNetMessage>(removedSelectors.size());
            synchronized (_selectorToMessage) {
                for (MessageSelector sel1 : removedSelectors) {
                    OutNetMessage msg = null;
                    List<OutNetMessage> msgs = null;
                    Object o = _selectorToMessage.remove(sel1);
                    if (o instanceof OutNetMessage) {
                        msg = (OutNetMessage)o;
                        rv.add(msg);
                    } else if (o instanceof List) {
                        msgs = (List<OutNetMessage>)o;
                        rv.addAll(msgs);
            }
        }

            }
        }
        return rv;
    }
    
    /**
     *  Registers a new, empty OutNetMessage, with the reply and timeout jobs specified.
     *  The onTimeout job is called at replySelector.getExpiration() (if no reply is received by then)
     *
     *  @param replySelector non-null; The same selector may be used for more than one message.
     *  @param onReply non-null
     *  @param onTimeout may be null
     *  @return a dummy OutNetMessage where getMessage() is null. Use it to call unregisterPending() later if desired.
     */
    public OutNetMessage registerPending(MessageSelector replySelector, ReplyJob onReply, Job onTimeout) {
        OutNetMessage msg = new OutNetMessage(_context);
        msg.setOnFailedReplyJob(onTimeout);
        msg.setOnReplyJob(onReply);
        msg.setReplySelector(replySelector);
        registerPending(msg, true);
        if (_log.shouldLog(Log.DEBUG))
            _log.debug("Registered: " + replySelector + " with reply job " + onReply +
                       " and timeout job " + onTimeout);
        return msg;
    }
    
    /**
     *  Register the message. Each message must have a non-null
     *  selector at msg.getReplySelector().
     *  The same selector may be used for more than one message.
     *
     *  @param msg msg.getMessage() and msg.getReplySelector() must be non-null
     */
    public void registerPending(OutNetMessage msg) { registerPending(msg, false); }

    /**
     *  @param allowEmpty is msg.getMessage() allowed to be null?
     */
    @SuppressWarnings("unchecked")
    private void registerPending(OutNetMessage msg, boolean allowEmpty) {
        if ( (!allowEmpty) && (msg.getMessage() == null) )
                throw new IllegalArgumentException("OutNetMessage doesn't contain an I2NPMessage? Impossible?");
        MessageSelector sel = msg.getReplySelector();
        if (sel == null) throw new IllegalArgumentException("No reply selector? Impossible?");


        synchronized (_selectorToMessage) { 
            Object oldMsg = _selectorToMessage.put(sel, msg);
            if (oldMsg != null) {
                List<OutNetMessage> multi = null;
                if (oldMsg instanceof OutNetMessage) {
                    //multi = Collections.synchronizedList(new ArrayList(4));
                    if (msg != oldMsg) {
                    multi = new ArrayList<OutNetMessage>(4);
                    multi.add((OutNetMessage)oldMsg);
                    multi.add(msg);
                    _selectorToMessage.put(sel, multi);
                    }
                } else if (oldMsg instanceof List) {
                    multi = (List<OutNetMessage>)oldMsg;
                    if (!multi.contains(msg)) {
                    multi.add(msg);
                    _selectorToMessage.put(sel, multi);
                }
                }
                if (_log.shouldLog(Log.WARN))
                    _log.warn("a single message selector [" + sel + "] with multiple messages ("+ multi + ")");
            }
        }
        _selectors.add(sel);
    }
    
    /**
     *  @param msg may be be null, if non-null should have a non-null selector
     */
    @SuppressWarnings("unchecked")
    public void unregisterPending(OutNetMessage msg) {
        if (msg == null) return;
        MessageSelector sel = msg.getReplySelector();
        if (sel == null) return;
        boolean stillActive = false;
        synchronized (_selectorToMessage) { 
            Object old = _selectorToMessage.remove(sel);
            if (old != null) {
                if (old instanceof List) {
                    List<OutNetMessage> l = (List<OutNetMessage>)old;
                    l.remove(msg);
                    if (!l.isEmpty()) {
                        _selectorToMessage.put(sel, l);
                        stillActive = true;
                    }
                }
            }
        }
        if (!stillActive)
            _selectors.remove(sel);
    }

    /** @deprecated unused */
    @Deprecated
    public void renderStatusHTML(Writer out) throws IOException {}
    
}
