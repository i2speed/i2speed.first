package net.i2p.router.transport.udp;

import net.i2p.data.DataHelper;
import net.i2p.data.Hash;
import net.i2p.util.Addresses;

/**
 * Unique ID for a peer - its IP + port, all bundled into a tidy obj.
 * If the remote peer is not reachable through an IP+port, this contains
 * the hash of their identity.
 *
 */
final class RemoteHostId {
    public byte _ip[];
    public int _port;
    private final Hash _peerHash;
    private int _hashCode;
    
    /** direct */
    public RemoteHostId(byte ip[], int port) {
        _peerHash = null;
        _ip = ip;
        _port = port;
        _hashCode = (port | (port << 16)) ^ DataHelper.fromInt4(_ip, (ip.length >> 1) - 2);
    }

    /** indirect */
    public RemoteHostId(Hash peerHash) {
        _peerHash = peerHash;
        _port = 0;
        _ip = null;
        _hashCode = DataHelper.hashCode(peerHash);
    }
    
    /** @return null if indirect */
    public byte[] getIP() { return _ip; }

    /** @return 0 if indirect */
    public int getPort() { return _port; }

    /** @return null if direct */
    public Hash getPeerHash() { return _peerHash; }
    
    @Override
    public int hashCode() {
        return _hashCode;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof RemoteHostId)) 
            return false;
        RemoteHostId id = (RemoteHostId)obj;

        if (_port != id._port) return false;
        if (_ip == null || id._ip == null) return DataHelper.eq(_peerHash, id._peerHash);
        int len = _ip.length + id._ip.length;
        if (len == 8) return _hashCode == id._hashCode;
        return (len == 32) && DataHelper.eq(_ip, id._ip);
    }
    
    @Override
    public String toString() {
        if (_ip != null) {
            return Addresses.toString(_ip, _port);
        } else {
            return _peerHash.toString();
        }
    }
}
