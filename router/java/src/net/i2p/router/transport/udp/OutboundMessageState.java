package net.i2p.router.transport.udp;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.atomic.*;

import net.i2p.I2PAppContext;
import net.i2p.data.Base64;
import net.i2p.data.i2np.I2NPMessage;
import net.i2p.router.OutNetMessage;
import net.i2p.router.transport.udp.PacketBuilder.Fragment;
import net.i2p.router.util.CDPQEntry;
import net.i2p.util.Log;

/**
 * Maintain the outbound fragmentation for resending, for a single message.
 *
 * All methods are thread-safe.
 *
 */
class OutboundMessageState implements CDPQEntry {
    private final I2PAppContext _context;
    private Log _log;
    /** may be null if we are part of the establishment */
    private final OutNetMessage _message;
    private final I2NPMessage _i2npMessage;
    /** will be null, unless we are part of the establishment */
    private final PeerState _peer;
    private long _expiration;
    private final byte[] _messageBuf;
    /** fixed fragment size across the message */
    private int _fragmentSize;
    /** bitmask, 0 if acked, all 0 = complete */
    private volatile long _fragmentAcks;
    private final int _numFragments;
    private long _startedOn;
    private int _pushCount;
    private int _maxSends;
    // we can't use the ones in _message since it is null for injections
    private long _enqueueTime;
    private long _seqNum;

    public static final int MAX_MSG_SIZE = 32 * 1024;

    private static final long EXPIRATION = 10*1000;

    /**
     *  "injected" message from the establisher.
     *
     *  Called from UDPTransport.
     *  @throws IllegalArgumentException if too big or if msg or peer is null
     */
    public OutboundMessageState(I2PAppContext context, I2NPMessage msg, PeerState peer) {
        this(context, null, msg, peer);
    }

    /**
     *  Normal constructor.
     *
     *  Called from OutboundMessageFragments.
     *  @throws IllegalArgumentException if too big or if msg or peer is null
     */
    public OutboundMessageState(I2PAppContext context, OutNetMessage m, PeerState peer) {
        this(context, m, m.getMessage(), peer);
    }

    static Log savedLog;
    static I2PAppContext savedContext;

    /**
     *  Internal.
     *  @param m null if msg is "injected"
     *  @throws IllegalArgumentException if too big or if msg or peer is null
     */
    private OutboundMessageState(I2PAppContext context, OutNetMessage m, I2NPMessage msg, PeerState peer) {
        if (msg == null || peer == null)
            throw new IllegalArgumentException();
        _context = context;
        if (context == savedContext)
            _log = savedLog;
        else {
            _log = _context.logManager().getLog(OutboundMessageState.class);
            savedLog = _log;
            savedContext = context;
        }
        _message = m;
        _i2npMessage = msg;
        _peer = peer;
        _startedOn = _context.clock().now();
        _expiration = _startedOn + EXPIRATION;
        //_expiration = msg.getExpiration();

        // now "fragment" it
        int totalSize = _i2npMessage.getRawMessageSize();
        if (totalSize > MAX_MSG_SIZE)
            throw new IllegalArgumentException("Size too large! " + totalSize);
        _messageBuf = new byte[totalSize];
        _i2npMessage.toRawByteArray(_messageBuf);
        _fragmentSize = _peer.fragmentSize();
        _numFragments = (totalSize + _fragmentSize - 1) / _fragmentSize;
        // This should never happen, as 534 bytes * 64 fragments > 32KB, and we won't bid on > 32KB
        if (_numFragments > InboundMessageState.MAX_FRAGMENTS)
            throw new IllegalArgumentException("Fragmenting a " + totalSize + " message into " + _numFragments + " fragments - too many!");
        // all 1's where we care
        _fragmentAcks = _numFragments < 64 ? mask(_numFragments) - 1L : -1L;
    }

    /**
     *  @param fragment 0-63
     */
    private static long mask(int fragment) {
        return 1L << fragment;
    }

    public OutNetMessage getMessage() { return _message; }

    public int getMessageId() { return (int)_i2npMessage.getUniqueId(); }

    public PeerState getPeer() { return _peer; }

    public boolean isExpired() {
        return _expiration < _context.clock().now();
    }

    /**
     * @since 0.9.38
     */
    public boolean isExpired(long now) {
        return _expiration < now;
    }

    public boolean isComplete() {
        return _fragmentAcks == 0;
    }

    public int getUnackedSize() {
        // if (isComplete())
        //     return 0; // concurrent ACK
        if (_pushCount == 0 || _numFragments == 1 || Long.bitCount(_fragmentAcks) == _numFragments)
            return _messageBuf.length; // shortcut for 99.9%, partial retransmit is ultra-rare
        int rv = Long.bitCount(_fragmentAcks) * _fragmentSize;
        if (((int)_fragmentAcks & 0x01) > 0) // adjust for possibly shorter first fragment
            rv -= _numFragments * _fragmentSize - _messageBuf.length;
        return rv;
    }

    public boolean needsSending(int fragment) {
        return (_fragmentAcks & mask(fragment)) != 0;
    }

    public long getLifetime() { return _context.clock().now() - _startedOn; }

    public long getStartedOn() { return _startedOn; }

    /**
     * Ack all the fragments in the ack list.
     *
     * @return true if the message was completely ACKed
     */
    public boolean acked(ACKBitfield bitfield) {
        // stupid brute force, but the cardinality should be trivial
        // better : _fragmentAcks &= ~bitfield._fragmentAcks
        int highest = bitfield.highestReceived();
        for (int i = 0; i <= highest && i < _numFragments; i++) {
            if (bitfield.received(i))
                _fragmentAcks &= ~mask(i);
        }
        return isComplete();
    }

    /**
     *  The max number of sends for any fragment, which is the
     *  same as the push count, at least as it's coded now.
     */
    public int getMaxSends() { return _maxSends; }

    /**
     *  The number of times we've pushed some fragments, which is the
     *  same as the max sends, at least as it's coded now.
     */
    public int getPushCount() { return _pushCount; }

    /**
     * Note that we have pushed the message fragments.
     * Increments push count (and max sends... why?)
     * @return true if this is the first push
     */
    public boolean push() {
        boolean rv = _pushCount == 0;
        if (rv) {
            _startedOn = _context.clock().now();
            _expiration = _startedOn + EXPIRATION;
        }
       // these will never be different...
        _pushCount++;
        _maxSends = _pushCount;
        return rv;
    }

    /**
     * How many fragments in the message
     */
    public int getFragmentCount() {
            return _numFragments;
    }

    /**
     * The size of the I2NP message. Does not include any SSU overhead.
     */
    public int getMessageSize() { return _messageBuf.length; }

    /**
     * The size in bytes of the fragment.
     * Does NOT include any SSU overhead.
     *
     * @param fragmentNum the number of the fragment
     * @return the size of the fragment specified by the number
     */
    public int fragmentSize(int fragmentNum) {
        if (_numFragments == 1)  //shortcut for typical case
            return _messageBuf.length;
        if (fragmentNum == 0) {
            int rv = _messageBuf.length % _fragmentSize;
            if (rv > 0) return rv;
        }
        return _fragmentSize;
    }

    /**
     * Write a part of the the message onto the specified buffer.
     *
     * @param out target to write
     * @param outOffset into outOffset to begin writing
     * @param fragmentNum fragment to write (0 indexed)
     * @return bytesWritten, NOT including packet overhead
     */
    public int writeFragment(byte out[], int outOffset, int fragmentNum) {
        int start;
        int toSend;
        if (fragmentNum == 0) {
            start = 0;
            toSend = fragmentSize(0);
        } else {
            start = _messageBuf.length - (_numFragments - fragmentNum) * _fragmentSize;
            toSend = _fragmentSize;
        }
        int end = start + toSend;
        if (end <= _messageBuf.length && outOffset + toSend <= out.length) {
            System.arraycopy(_messageBuf, start, out, outOffset, toSend);
            return toSend;
        } else {
            if (_log.shouldLog(Log.WARN))
                _log.warn("Error: " + start + '/' + end + '/' + outOffset + '/' + out.length);
        }
        return -1;
    }

    /**
     *  For CDQ
     *  @since 0.9.3
     */
    public void setEnqueueTime(long now) {
        _enqueueTime = now;
    }

    /**
     *  For CDQ
     *  @since 0.9.3
     */
    public long getEnqueueTime() {
        return _enqueueTime;
    }

    /**
     *  For CDQ
     *  @since 0.9.3
     */
    public void drop() {
        _peer.getTransport().failed(this, false);
    }

    /**
     *  For CDPQ
     *  @since 0.9.3
     */
    public void setSeqNum(long num) {
        _seqNum = num;
    }

    /**
     *  For CDPQ
     *  @since 0.9.3
     */
    public long getSeqNum() {
        return _seqNum;
    }

    /**
     *  For CDPQ
     *  @return OutNetMessage priority or 1000 for injected
     *  @since 0.9.3
     */
    public int getPriority() {
        return _message != null ? _message.getPriority() : 1000;
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder(256);
        buf.append("OB Message ").append(_i2npMessage.getUniqueId());
        buf.append(" type ").append(_i2npMessage.getType());
        buf.append(" with ").append(_numFragments).append(" fragments");
        buf.append(" of size ").append(_messageBuf.length);
        buf.append(" volleys: ").append(_maxSends);
        buf.append(" lifetime: ").append(getLifetime());
        if (!isComplete()) {
            buf.append(" pending fragments: ");
            for (int i = 0; i < _numFragments; i++) {
                if (needsSending(i))
                    buf.append(i).append(' ');
            }
        }
        //buf.append(" to: ").append(_peer.toString());
        return buf.toString();
    }
}
