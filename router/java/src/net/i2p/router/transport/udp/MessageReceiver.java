package net.i2p.router.transport.udp;

import java.util.concurrent.BlockingQueue;

import net.i2p.data.Base64;
import net.i2p.data.ByteArray;
import net.i2p.data.i2np.DatabaseStoreMessage;
import net.i2p.data.i2np.I2NPMessage;
import net.i2p.data.i2np.I2NPMessageException;
import net.i2p.data.i2np.I2NPMessageHandler;
import net.i2p.data.i2np.I2NPMessageImpl;
import net.i2p.router.RouterContext;
import net.i2p.util.HexDump;
import net.i2p.util.Log;
import net.i2p.router.JobImpl;

/**
 * Pull fully completed fragments off the {@link InboundMessageFragments} queue,
 * parse 'em into I2NPMessages, and stick them on the 
 * {@link net.i2p.router.InNetMessagePool} by way of the {@link UDPTransport}.
 */
class MessageReceiver {
    private final RouterContext _context;
    private final Log _log;
    private final UDPTransport _transport;
    private volatile boolean _alive;
    private I2NPMessageHandler _handler;
    private ByteArray _buf;
    
    public MessageReceiver(RouterContext ctx, UDPTransport transport) {
        _context = ctx;
        _log = ctx.logManager().getLog(MessageReceiver.class);
        _transport = transport;
        _alive = true;
        _handler = new I2NPMessageHandler(_context);
        _buf = new ByteArray(new byte[I2NPMessage.MAX_SIZE]);
        }
    
    public synchronized void startup() {
        _alive = true;
    }
    
    public synchronized void shutdown() {
        _alive = false;
    }

    /**
     *  This does the actual receive.
     */
    public void receiveMessage(InboundMessageState state) {

        final InboundMessageState message = state;
    
        if (_alive) {
            try {
                I2NPMessage msg = readMessage(_buf, message, _handler);
                if (msg != null)
                    _transport.messageReceived(msg, null, message.getFrom(), message.getLifetime(), message.getCompleteSize());
            } catch (RuntimeException re) {
                _log.error("b0rked receiving a message.. wazza huzza hmm?", re);
            }
        }
    }
    
    /**
     *  Assemble all the fragments into an I2NP message.
     *  This calls state.releaseResources(), do not access state after calling this.
     *
     *  @param buf temp buffer for convenience
     *  @return null on error
     */
    private I2NPMessage readMessage(ByteArray buf, InboundMessageState state, I2NPMessageHandler handler) {
        try {
            //byte buf[] = new byte[state.getCompleteSize()];
            I2NPMessage m;
            int numFragments = state.getFragmentCount();
            if (numFragments > 1) {
                ByteArray fragments[] = state.getFragments();
                int off = 0;
                for (int i = 0; i < numFragments; i++) {
                    System.arraycopy(fragments[i].getData(), 0, buf.getData(), off, fragments[i].getValid());
                    //if (_log.shouldLog(Log.DEBUG))
                    //    _log.debug("Raw fragment[" + i + "] for " + state.getMessageId() + ": " 
                    //               + Base64.encode(fragments[i].getData(), 0, fragments[i].getValid())
                    //               + " (valid: " + fragments[i].getValid() 
                    //               + " raw: " + Base64.encode(fragments[i].getData()) + ")");
                    off += fragments[i].getValid();
                }
                if (off != state.getCompleteSize()) {
                    if (_log.shouldLog(Log.WARN))
                        _log.warn("Hmm, offset of the fragments = " + off + " while the state says " + state.getCompleteSize());
                    return null;
                }
                //if (_log.shouldLog(Log.DEBUG))
                //    _log.debug("Raw byte array for " + state.getMessageId() + ": " + HexDump.dump(buf.getData(), 0, state.getCompleteSize()));
                m = I2NPMessageImpl.fromRawByteArray(_context, buf.getData(), 0, state.getCompleteSize(), handler);
            } else {
                // zero copy for single fragment
                m = I2NPMessageImpl.fromRawByteArray(_context, state.getFragments()[0].getData(), 0, state.getCompleteSize(), handler);
            }
            m.setUniqueId(state.getMessageId());
            return m;
        } catch (I2NPMessageException ime) {
            if (_log.shouldLog(Log.WARN)) {
                ByteArray ba;
                if (state.getFragmentCount() > 1)
                    ba = buf;
                else
                    ba = state.getFragments()[0];
                byte[] data = ba.getData();
                _log.warn("Message invalid: " + state +
                          " PeerState: " + _transport.getPeerState(state.getFrom()) +
                          "\nDUMP:\n" + HexDump.dump(data, 0, state.getCompleteSize()) +
                          "\nRAW:\n" + Base64.encode(data, 0, state.getCompleteSize()),
                          ime);
            }
            if (state.getFragments()[0].getData()[0] == DatabaseStoreMessage.MESSAGE_TYPE) {
                PeerState ps = _transport.getPeerState(state.getFrom());
                if (ps != null && ps.getRemotePort() == 65520) {
                    // distinct port of buggy router
                    _transport.sendDestroy(ps);
                    _transport.dropPeer(ps, true, "Corrupt DSM");
                    _context.banlist().banlistRouterForever(state.getFrom(),
                                                            "Sent corrupt message");  // don't bother translating
                }
            }
            _context.messageHistory().droppedInboundMessage(state.getMessageId(), state.getFrom(), "error: " + ime.toString() + ": " + state.toString());
            return null;
        } catch (RuntimeException e) {
            // e.g. AIOOBE
            if (_log.shouldLog(Log.WARN))
                _log.warn("Error handling a message: " + state, e);
            _context.messageHistory().droppedInboundMessage(state.getMessageId(), state.getFrom(), "error: " + e.toString() + ": " + state.toString());
            return null;
        } finally {
            state.releaseResources();
        }
    }
}
