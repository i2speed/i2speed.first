package net.i2p.router.transport.udp;

import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import net.i2p.router.RouterContext;
import net.i2p.util.I2PThread;
import net.i2p.util.Log;
import net.i2p.util.ConcurrentHashSet;

/**
 * Blocking thread that is given peers by the inboundFragment pool, sending out
 * any outstanding ACKs.  
 * The ACKs are sent directly to UDPSender,
 * bypassing OutboundMessageFragments and PacketPusher.
 */
class ACKSender implements Runnable {
    private final RouterContext _context;
    private final Log _log;
    private final UDPTransport _transport;
    private final PacketBuilder _builder;
    private volatile boolean _alive;
    private Iterator<PeerState> _iterator;
    /** list of peers (PeerState) who we have received data from but not yet ACKed to */
    private Set<PeerState> _peersToACK;
    
    /** how frequently do we want to send ACKs to a peer? */
    static final int ACK_FREQUENCY = 150;
    static final int SLEEP_TIME = 5 + ACK_FREQUENCY / 3;
    static final int SLEEP_TIME_DIV_2 = SLEEP_TIME / 2;
    
    public ACKSender(RouterContext ctx, UDPTransport transport) {
        _context = ctx;
        _log = ctx.logManager().getLog(ACKSender.class);
        _transport = transport;
        _builder = new PacketBuilder(_context, transport);
        _alive = true;
        _peersToACK = new ConcurrentHashSet<PeerState>();
        _iterator = _peersToACK.iterator();
    }
    
    /**
     *  Add to the queue.
     */
    public void ackPeer(PeerState peer) {
        if (_alive)
            _peersToACK.add(peer);
    }
    
    public synchronized void startup() {
        _alive = true;
        // I2PThread t = new I2PThread(this, "UDP ACK sender", true);
        // t.start();
    }
    
    public synchronized void shutdown() { 
        _alive = false;
            try {
            Thread.sleep(ACK_FREQUENCY);
            } catch (InterruptedException ie) {}
        _peersToACK.clear();
    }
    
    private static long ackFrequency(long timeSinceACK, long rtt) {
        // if we are actively pumping lots of data to them, we can depend upon
        // the unsentACKThreshold to figure out when to send an ACK instead of
        // using the timer, so we can set the timeout/frequency higher
        if (timeSinceACK < 2*1000)
            return Math.max(rtt/2, ACK_FREQUENCY);
        else
            return ACK_FREQUENCY;
    }
    
    public void run() {
        try {
            run2();
        } finally {
            // prevent OOM on thread death
            if (_alive) {
                _alive = false;
                _log.error("ACK Sender died");
            }
        }
    }

    private void run2() {
        while (_alive) {
            PeerState peer = null;
            int remaining = _peersToACK.size();
            int count = 0;
            long wanted = 0;
            long now = _context.clock().now();

            while (_alive && count < remaining) {
                count++;
                if (!_iterator.hasNext())
                    _iterator = _peersToACK.iterator();
                peer = _iterator.next();
//                wanted = peer.getWantedACKSendSince();
                if (wanted <= 0) {
                    // it got acked by somebody - discard and go around again
                    // ignore very small chance of race, we are ACKing frequently
                    _peersToACK.remove(peer);
                    peer = null;
                    remaining--;
                    continue;
                }
                long delta = wanted + ackFrequency(now - peer.getLastACKSend(), peer.getRTT()) - now;
                if ( (delta  - SLEEP_TIME_DIV_2 <= 0) || (peer.unsentACKThresholdReached()) ) {
                            // found one to ack
                    _peersToACK.remove(peer);
                            break;
                }
                            // not yet, go around again
                        if (_log.shouldLog(Log.DEBUG))
                    _log.debug("Pending ACK (delta = " + delta + ") for " + peer);
            } // inner while()

            if (peer == null) {
                if (_log.shouldLog(Log.DEBUG))
                    _log.debug("sleeping, pending size = " + remaining);
                        try {
                            // sleep a little longer than the divided frequency,
                            // so it will be ready after we circle around a few times
                    Thread.sleep(SLEEP_TIME);
                        } catch (InterruptedException ie) {}
                continue;
            }
                    
            if (peer != null) {
                // set above before the break
                //long wanted = peer.getWantedACKSendSince();
                if (wanted <= 0) {
                    if (_log.shouldLog(Log.WARN))
                        _log.warn("why are we acking something they dont want?  remaining=" + remaining + ", peer=" + peer);
                    continue;
                }
                
                UDPPacket ack = _builder.buildACK(peer);
//                ack.markType(1);
                ack.setFragmentCount(-1);
                ack.setMessageType(PacketBuilder.TYPE_ACK);
                
                if (_log.shouldLog(Log.INFO))
                    _log.info("Sending ACKs to " + peer);
                // locking issues, we ignore the result, and acks are small,
                // so don't even bother allocating
                //peer.allocateSendingBytes(ack.getPacket().getLength(), true);
                // ignore whether its ok or not, its a bloody ack.  this should be fixed, probably.
                _transport.send(ack);
                
                if ( (wanted > 0) && (peer.hasACKs()) ) {
                    // still full packets left to be ACKed, since wanted time
                    // is reset by retrieveACKBitfields when all of the IDs are
                    // removed
                    if (_log.shouldInfo())
                        _log.info("Precautionary rerequest ACK for peer " + peer);
                    ackPeer(peer);
                }
            }
        }
    }
}
