package net.i2p.router.transport.ntcp;

import net.i2p.router.RouterContext;
import net.i2p.util.Log;

/**
 * Pool of running threads which will transform the next I2NP message into
 * something ready to be transferred over an NTCP connection, including the
 * encryption of the data read.
 *
 */
class Writer {
    private final Log _log;
    private final EventPumper _pumper;
    
    public Writer(RouterContext ctx, EventPumper pumper) {
        _log = ctx.logManager().getLog(getClass());
        _pumper = pumper;
    }
    
    public synchronized void startWriting(int numWriters) {
        /**
        for (int i = 1; i <=numWriters; i++) {
            Runner r = new Runner();
            I2PThread t = new I2PThread(r, "NTCP writer " + i + '/' + numWriters, true);
            _runners.add(r);
            t.start();
        }
        */
    }

    public synchronized void stopWriting() {
    }
    
    public void addWrite(NTCPConnection con, String source) {
        _pumper.wakeUp(); // expensive, should not go synced
        if (_log.shouldLog(Log.DEBUG))
            _log.debug("addWrite: " + con + " : " + source);
    }

    public void connectionClosed(NTCPConnection con) {
//        synchronized (_pendingConnections) {
//            _pendingConnections.remove(con);
//        }
    }
    
}
