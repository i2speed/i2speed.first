package net.i2p.router.transport.ntcp;

import java.nio.ByteBuffer;

import net.i2p.router.RouterContext;
import net.i2p.util.Log;

/**
 * Pool of running threads which will process any read bytes on any of the 
 * NTCPConnections, including the decryption of the data read, connection
 * handshaking, parsing bytes into I2NP messages, etc.
 *
 */
class Reader {
    private final Log _log;
    private final RouterContext context;
    private volatile boolean _stop;
    private volatile NTCPConnection _closedCon;

    public Reader(RouterContext ctx) {
        context = ctx;
        _log = ctx.logManager().getLog(getClass());
        _stop = true;
        _closedCon = null;
    }
    
    public synchronized void startReading(int numReaders) {
        if (_log.shouldLog(Log.INFO)) _log.info("Starting reader");
        _stop = false;
    }

    public synchronized void stopReading() {
        if (_log.shouldLog(Log.INFO)) _log.info("Stopping reader");
        _stop = true;
    }
    
    public void wantsRead(NTCPConnection con, ByteBuffer buf) {
        if (!_stop) {
            if (_log.shouldLog(Log.DEBUG))
                _log.debug("begin read for " + con);
            try {
                processRead(con, buf);
            } catch (IllegalStateException ise) {
                // FailedEstablishState.receive() (race - see below)
                if (_log.shouldWarn())
                    _log.warn("Error in the ntcp reader", ise);
            } catch (RuntimeException re) {
                _log.log(Log.ERROR, "Error in the ntcp reader", re);
            }
            if (_log.shouldLog(Log.DEBUG))
                _log.debug("end read for " + con);
        }
    }

    public void connectionClosed(NTCPConnection con) {
        _closedCon = con;
    }
    
    /**
     * Process everything read.
     * Return read buffers back to the pool as we process them.
     */
    private void processRead(NTCPConnection con, ByteBuffer buf) {
        if (con == _closedCon || con.isClosed())
            return;
        if (con.isEstablished())
            con.recvEncryptedI2NP(buf);
        else {
            EstablishState est = con.getEstablishState();
            if (est.isComplete()) {
                // why is it complete yet !con.isEstablished?
                _log.error("establishment state [" + est + "] is complete, yet the connection isn't established? " 
                        + con.isEstablished() + " (inbound? " + con.isInbound() + " " + con + ")");
                return;
            }
            // FIXME call est.isCorrupt() before also? throws ISE here... see above
            est.receive(buf);
            if (est.isCorrupt())
                con.close();
            // EstablishState is responsible for passing "extra" data to the con
        }
    }
}
