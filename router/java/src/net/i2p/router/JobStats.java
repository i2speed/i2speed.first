package net.i2p.router;

/**
 *  Glorified struct to contain basic job stats.
 *  Public for router console only.
 *  For use by the router only. Not to be used by applications or plugins.
 */
public class JobStats {
    private final String _job;
    private int _numRuns;
    private int _numDropped;
    private long _totalTime;
    private int _maxTime;
    private int _minTime;
    private long _totalPendingTime;
    private int _maxPendingTime;
    private int _minPendingTime;
    
    public JobStats(String key) {
        _job = key;
        _minTime = Integer.MAX_VALUE;
        _maxPendingTime = -1;
        _minPendingTime = Integer.MAX_VALUE;
    }
    
    public void jobRan(int runTime, int lag) {
        _numRuns++;
        _totalTime += runTime;
        if (runTime > _maxTime)
            _maxTime = runTime;
        if (runTime < _minTime)
            _minTime = runTime;
        _totalPendingTime += lag;
        if (lag > _maxPendingTime)
            _maxPendingTime = lag;
        if (lag < _minPendingTime)
            _minPendingTime = lag;
    }
    
    /** @since 0.9.19 */
    public void jobDropped() {
        _numDropped++;
    }

    /** @since 0.9.19 */
    public long getDropped() { return _numDropped; }

    public String getName() { return _job; }
    public long getRuns() { return _numRuns; }
    public long getTotalTime() { return _totalTime; }
    public long getMaxTime() { return _maxTime; }
    public long getMinTime() { return _minTime; }

    public float getAvgTime() { 
        if (_numRuns > 0) 
            return _totalTime / (float)_numRuns; 
        else 
            return 0; 
    }
    public long getTotalPendingTime() { return _totalPendingTime; }
    public int getMaxPendingTime() { return _maxPendingTime; }
    public int getMinPendingTime() { return _minPendingTime; }

    public float getAvgPendingTime() { 
        if (_numRuns > 0) 
            return _totalPendingTime / (float)_numRuns; 
        else 
            return 0; 
    }
    
/****
    @Override
    public int hashCode() { return _job.hashCode(); }

    @Override
    public boolean equals(Object obj) {
        if ( (obj != null) && (obj instanceof JobStats) ) {
            JobStats stats = (JobStats)obj;
            return DataHelper.eq(getName(), stats.getName()) &&
                   getRuns() == stats.getRuns() &&
                   getTotalTime() == stats.getTotalTime() &&
                   getMaxTime() == stats.getMaxTime() &&
                   getMinTime() == stats.getMinTime();
        } else {
            return false;
        }
    }
    
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append("Over ").append(getRuns()).append(" runs, job <b>").append(getName()).append("</b> took ");
        buf.append(getTotalTime()).append("ms (").append(getAvgTime()).append("ms/").append(getMaxTime()).append("ms/");
        buf.append(getMinTime()).append("ms avg/max/min) after a total lag of ");
        buf.append(getTotalPendingTime()).append("ms (").append(getAvgPendingTime()).append("ms/");
        buf.append(getMaxPendingTime()).append("ms/").append(getMinPendingTime()).append("ms avg/max/min)");
        return buf.toString();
    }
****/
}
