package net.i2p.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import net.i2p.I2PAppContext;
import net.i2p.data.Hash;

import com.wolfssl.wolfcrypt.Sha256;
import com.wolfssl.wolfcrypt.FeatureDetect;


/** 
 * Defines a wrapper for SHA-256 operation.
 * 
 */
public final class SHA256Generator {

    private final static boolean _native = FeatureDetect.Sha256Enabled();

    // Constants for each round
    private static final int[] ROUND_CONSTS = {
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
        0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
        0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
        0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
        0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
        0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
        0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
        0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
        0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
        0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
        0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
        0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
        0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
        0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
        0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    };

    private static int MAXSIZE = 391; // frequent hits seen for this size
    private static int CACHESIZE = 2048;
    private static int MASK = CACHESIZE - 1;
    private volatile boolean cacheinit;
    private static final SHA256Container[] SHA256Cache = new SHA256Container[CACHESIZE];

    /**
     *  @param context unused
     */
    public SHA256Generator(I2PAppContext context) {
        if (_native) System.out.println("Using WolfCrypt SHA256");
        if (!cacheinit)
            synchronized (SHA256Cache) {
                if (!cacheinit) {
                    cacheinit = true;
                    SHA256Container dummy = new SHA256Container();
                    // force initial cache misses
                    dummy.data1 = new byte[MAXSIZE + 1];
                    dummy.data2 = dummy.data1;
                    for (int i = 0; i < CACHESIZE; i++) 
                        SHA256Cache[i] = dummy;
                }
            }

    }
    
    public static final SHA256Generator getInstance() {
        return I2PAppContext.getGlobalContext().sha();
    }
    
    private static class SHA256Container {
        int print1;
        byte[] data1;
        byte[] hash1;
        int print2;
        byte[] data2;
        byte[] hash2;
        public SHA256Container() {
        };
    }

    /**
     * Calculate the SHA-256 hash of the source and cache the result.
     * @param source what to hash
     * @return hash of the source
     */
    public final Hash calculateHash(byte[] source) {
        return calculateHash(source, 0, source.length);
    }

    /**
     * Calculate the hash.
     * @param source what to hash
     */
    public final Hash calculateHash(byte[] source, int start, int len) {
        return Hash.create(docalc(source, start, len, null, 0));
    }

    /**
     * Use this if you only need the data, not a Hash object.
     * @param out needs 32 bytes starting at outOffset
     */
    public final void calculateHash(byte[] source, int start, int len, byte out[], int outOffset) {
        docalc(source, start, len, out, outOffset);
    }

    /**
     * Use this if you need the data as an array
     */
    public final byte[] calculateHashArray(byte[] source, int start, int len) {
        return docalc(source, start, len, null, 0);
    }

    /**
     * @param out non-null if bytes wanted instead of byte array
     */
    public final byte[] docalc(byte[] source, int start, int len, byte out[], int outOffset) {
        if ((start < 0) || (len < 0) || (start > source.length - len))
            throw new ArrayIndexOutOfBoundsException();
        boolean cacheme = len <= MAXSIZE;
        if (_native && !cacheme) {
            Sha256 sha = new Sha256();
            sha.update(source, start, len);
            if (out == null)
                return sha.digest();
            System.arraycopy(sha.digest(), 0, out, outOffset, Hash.HASH_LENGTH);
            return null;
        }
        SHA256Container temp = null, temp2;

        int[] table = new int[64];
        int s0 = 0x6a09e667;
        int s1 = 0xbb67ae85;
        int s2 = 0x3c6ef372;
        int s3 = 0xa54ff53a;
        int s4 = 0x510e527f;
        int s5 = 0x9b05688c;
        int s6 = 0x1f83d9ab;
        int s7 = 0x5be0cd19;

        int offset = start;
        int boundary = start + len;
        int lastround = (len + 8) >> 6;
        boolean padded = false;
        int index = 0;
        int print = 0;
        for (int round = 0; round <= lastround; round++) {
            int numints = Math.min(16, (boundary - offset) >> 2);
            int i;
            for (i = 0; i < numints; i++)
                table[i] = source[offset++] << 24 | (source[offset++] & 0xFF) << 16 | (source[offset++] & 0xFF) << 8 | (source[offset++] & 0xFF);
            if (i < 16) {
                if (!padded) {
                    padded = true;
                    switch (boundary - offset) {
                        case 0:
                            table[i++] = 0x80000000;
                            break;
                        case 1:
                            table[i++] = source[offset] << 24 | 0x800000;
                            break;
                        case 2:
                            table[i++] = source[offset++] << 24 | (source[offset] & 0xFF) << 16 | 0x8000;
                            break;
                        case 3:
                            table[i++] = source[offset++] << 24 | (source[offset++] & 0xFF) << 16 | (source[offset] & 0xFF) << 8 | 0x80;
                    }
                }
                // this is good for > 500 MB
                //                    long bits = len << 3;
                //                    table[14] = (int)(bits >>> 32);
                if (i < 16) {
                    for (int j = i; j < 15; j++)
                        table[j] = 0;
                    table[15] = i <= 14 ? len << 3 : 0;
                }
            }

            if (round == 0 && cacheme) {
                print = table[0];
                for (i = 1; i < 16; i++)
                    print ^= table[i];
                index = print & MASK;
                temp = SHA256Cache[index];
                boolean test1 = temp.print1 == print && temp.data1.length == len;
                boolean test2 = temp.print2 == print && temp.data2.length == len;

                if (test1 || test2) {
                    byte[] tempdata = test1 ? temp.data1 : temp.data2;
                    for (i = 0; i < len; i++)
                        if (tempdata[i] != source[start + i])
                            break;
                    if (i == len) {
                        if (test2) {
                            temp2 = new SHA256Container();
                            temp2.print1 = temp.print2;
                            temp2.hash1 = temp.hash2;
                            temp2.data1 = temp.data2;
                            temp2.print2 = temp.print1;
                            temp2.hash2 = temp.hash1;
                            temp2.data2 = temp.data1;
                            SHA256Cache[index] = temp2;
                        }
                        byte[] rv = test1 ? temp.hash1 : temp.hash2;
                        if (out == null) return rv;
                        System.arraycopy(rv, 0, out, outOffset, Hash.HASH_LENGTH);
                        return null;
                    }
                }
            }

            int i1 = 14;
            int i2 = 9;
            int i3 = 1;
            int i4 = 0;
            for (i = 16; i < 64; i++) {
                int p1 = table[i1++];
                int p3 = table[i3++];
                table[i] = ((p1 >>> 17 | p1 << 15) ^ (p1 >>> 19 | p1 << 13) ^ p1 >>> 10) + table[i2++] +
                    ((p3 >>> 7 | p3 << 25) ^ (p3 >>> 18 | p3 << 14) ^ p3 >>> 3) + table[i4++];
            }

            int a = s0;
            int b = s1;
            int c = s2;
            int d = s3;
            int e = s4;
            int f = s5;
            int g = s6;
            int h = s7;

            for (i = 0; i < 64; i++) {
                int T1 = h + ((e >>> 6 | e << 26) ^ (e >>> 11 | e << 21) ^ (e >>> 25 | e << 7)) + (e & f ^ ~e & g) + ROUND_CONSTS[i] + table[i];
                int T2 = ((a >>> 2 | a << 30) ^ (a >>> 13 | a << 19) ^ (a >>> 22 | a << 10)) + (a & b ^ a & c ^ b & c);
                h = g;
                g = f;
                f = e;
                e = d + T1;
                d = c;
                c = b;
                b = a;
                a = T1 + T2;
            }
            s0 += a;
            s1 += b;
            s2 += c;
            s3 += d;
            s4 += e;
            s5 += f;
            s6 += g;
            s7 += h;
        }

        if (!cacheme && out != null) {
            offset = outOffset + 32;
            for (int i = 0; i < 4; i++) {out[--offset] = (byte)(s7); s7 >>= 8;}
            for (int i = 0; i < 4; i++) {out[--offset] = (byte)(s6); s6 >>= 8;}
            for (int i = 0; i < 4; i++) {out[--offset] = (byte)(s5); s5 >>= 8;}
            for (int i = 0; i < 4; i++) {out[--offset] = (byte)(s4); s4 >>= 8;}
            for (int i = 0; i < 4; i++) {out[--offset] = (byte)(s3); s3 >>= 8;}
            for (int i = 0; i < 4; i++) {out[--offset] = (byte)(s2); s2 >>= 8;}
            for (int i = 0; i < 4; i++) {out[--offset] = (byte)(s1); s1 >>= 8;}
            for (int i = 0; i < 4; i++) {out[--offset] = (byte)(s0); s0 >>= 8;}
            return null;
        }
        byte[] rv = new byte[Hash.HASH_LENGTH];
        offset = 32;
        for (int i = 0; i < 4; i++) {rv[--offset] = (byte)(s7); s7 >>= 8;}
        for (int i = 0; i < 4; i++) {rv[--offset] = (byte)(s6); s6 >>= 8;}
        for (int i = 0; i < 4; i++) {rv[--offset] = (byte)(s5); s5 >>= 8;}
        for (int i = 0; i < 4; i++) {rv[--offset] = (byte)(s4); s4 >>= 8;}
        for (int i = 0; i < 4; i++) {rv[--offset] = (byte)(s3); s3 >>= 8;}
        for (int i = 0; i < 4; i++) {rv[--offset] = (byte)(s2); s2 >>= 8;}
        for (int i = 0; i < 4; i++) {rv[--offset] = (byte)(s1); s1 >>= 8;}
        for (int i = 0; i < 4; i++) {rv[--offset] = (byte)(s0); s0 >>= 8;}
        if (!cacheme) return rv;

        temp2 = new SHA256Container();
        temp2.print2 = temp.print1;
        temp2.hash2 = temp.hash1;
        temp2.data2 = temp.data1;
        temp2.print1 = print;
        temp2.hash1 = rv;
        temp2.data1 = new byte[len];
        System.arraycopy(source, start, temp2.data1, 0, len);
        SHA256Cache[index] = temp2;
        if (out == null) return rv;
        System.arraycopy(rv, 0, out, outOffset, Hash.HASH_LENGTH);
        return null;
    }
    
    /**
     *  Return a new MessageDigest from the system libs.
     *  @since 0.8.7, public since 0.8.8 for FortunaStandalone
     */
    public static MessageDigest getDigestInstance() {
        try {
            return MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: SHA256Generator 'text to hash'");
            System.exit(1);
        }
        System.out.println(net.i2p.data.Base64.encode(getInstance().calculateHash(net.i2p.data.DataHelper.getUTF8(args[0])).getData()));
    }
}
