package net.i2p.crypto;

class AESContainer {
    int p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15;

    public void fill(byte[] in, int inOffset) {
        p0 = in[inOffset++];
        p1 = in[inOffset++];
        p2 = in[inOffset++];
        p3 = in[inOffset++];
        p4 = in[inOffset++];
        p5 = in[inOffset++];
        p6 = in[inOffset++];
        p7 = in[inOffset++];
        p8 = in[inOffset++];
        p9 = in[inOffset++];
        p10 = in[inOffset++];
        p11 = in[inOffset++];
        p12 = in[inOffset++];
        p13 = in[inOffset++];
        p14 = in[inOffset++];
        p15 = in[inOffset];
    }

    public void fill(byte[] in) {
        p0 = in[0];
        p1 = in[1];
        p2 = in[2];
        p3 = in[3];
        p4 = in[4];
        p5 = in[5];
        p6 = in[6];
        p7 = in[7];
        p8 = in[8];
        p9 = in[9];
        p10 = in[10];
        p11 = in[11];
        p12 = in[12];
        p13 = in[13];
        p14 = in[14];
        p15 = in[15];
    }

    public void dump(byte[] out, int outIndex) {
        out[outIndex++] = (byte)p0;
        out[outIndex++] = (byte)p1;
        out[outIndex++] = (byte)p2;
        out[outIndex++] = (byte)p3;
        out[outIndex++] = (byte)p4;
        out[outIndex++] = (byte)p5;
        out[outIndex++] = (byte)p6;
        out[outIndex++] = (byte)p7;
        out[outIndex++] = (byte)p8;
        out[outIndex++] = (byte)p9;
        out[outIndex++] = (byte)p10;
        out[outIndex++] = (byte)p11;
        out[outIndex++] = (byte)p12;
        out[outIndex++] = (byte)p13;
        out[outIndex++] = (byte)p14;
        out[outIndex] = (byte)p15;
    }
}

