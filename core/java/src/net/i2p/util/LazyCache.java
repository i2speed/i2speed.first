package net.i2p.util;

import java.util.Collection;
import java.util.Map;
import java.lang.Thread;

/*
 * Free software
 */


/**
 * 
 * @param <E> the type of elements held in this collection
 */
public class LazyCache<E>{

    public static interface ObjectFactory<E> {
        E newInstance();
    }
    
    private final ObjectFactory<E> factory;

    private LazyConcurrentMap<Thread, E> map;
    
    public LazyCache(ObjectFactory<E> factory) {
        this.factory = factory;
        map = new LazyConcurrentMap<Thread, E>(16);
    }

    public E get() {
        Thread t = Thread.currentThread();
        E e = map.get(t);
        if (e == null) {
            e = factory.newInstance();
            map.put(t, e);
        }
        return e;
    }

}