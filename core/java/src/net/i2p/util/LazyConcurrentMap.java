package net.i2p.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Collection;

/**
 *  A concurrent map for infrequent updates
 */
public class LazyConcurrentMap<K, V> {
    private Map<K, V> map;
    private final int _max;


    public LazyConcurrentMap(int max) {
        _max = max;
        map = new HashMap<K, V>(_max);
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public int size() {
        return map.size();
    }

    public boolean containsKey(K key) {
        return map.containsKey(key);
    }

    public boolean containsValue(K key) {
        return map.containsValue(key);
    }

    public void clear() {
        map = new HashMap<K, V>(_max);
    }

    public V get(K key) {
        return (V)map.get(key);
    }

    public Map<K, V> map() {
        return (Map<K, V>)map;
    }

    public V put(K key, V value) {
        Map<K, V> oldmap = map;
        V oldval = oldmap.get(key);
        if (oldval == value) return value;
        Map<K, V> newmap = new HashMap<K, V>(_max);
        newmap.putAll(oldmap);
        newmap.put(key, value);
        map = newmap;
        return oldval;
    }

    public void putAll(Map<K, V> C) {
        Map<K, V> oldmap = map;
        Map<K, V> newmap = new HashMap<K, V>(_max);
        newmap.putAll(oldmap);
        newmap.putAll(C);
        map = newmap;
    }

    public V remove(K key) {
        Map<K, V> oldmap = map;
        V oldval;
        if (!oldmap.containsKey(key)) return null;
        Map<K, V> newmap = new HashMap<K, V>(_max);
        if (oldmap.size() > 1) {
            newmap.putAll(oldmap);
            oldval = newmap.remove(key);
            map = newmap;
        } else {
            map = newmap;
            oldval = oldmap.remove(key);
        } 
        return oldval;
    }

    public V putIfAbsent(K key, V value) {
        V v = get(key);
        if (v == null) {
            Map<K, V> oldmap = map;
            Map<K, V> newmap = new HashMap<K, V>(_max);
            newmap.putAll(oldmap);
            v = newmap.put(key, value);
            map = newmap;
        }
         return v;
    }
    public Collection<V> values() {
        return (Collection<V>)map.values();
    }
/*
    public Iterator<K, V> iterator() {
        return map.get().iterator();
    }
*/

}
