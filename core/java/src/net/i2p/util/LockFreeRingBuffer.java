package net.i2p.util;

import java.util.Collection;

/*
 * Free software
 */

import java.util.concurrent.atomic.*;


/**
 * This implements part of the queue interface and
 * replaces concurrent queues as well as hashsets when used to hand over items from thread A to B
 * should be sized to hold at least 200 ms of traffic due to effects described below
 * in case of a race poll() will not wait for offer() to complete
 * minimum 32 items enforced
 * 
 * @param <E> the type of elements held in this collection
 */
public class LockFreeRingBuffer<E>{

    static final int MINBUFFER = 16;

    private int ceilingNextPowerOfTwo(int x) {
        // From Hacker's Delight, Chapter 3, Harry S. Warren Jr.
        return 1 << (Integer.SIZE - Integer.numberOfLeadingZeros(x - 1));
    }

    public static interface ObjectFactory<E> {
        E newInstance();
    }
    
    private final ObjectFactory<E> factory;
    
    Object[] array;
    /* head pointing at first field valid */
    int head;
    /* tail pointing past last field valid */
    int tail;
    private final int mincapacity;
    private final int maxcapacity;
    private int capacity;
    private int mask;
    private final AtomicBoolean offer_lock = new AtomicBoolean();
    private final AtomicBoolean poll_lock = new AtomicBoolean();
    private final AtomicInteger size = new AtomicInteger();
    private boolean taking = false;
    private boolean idle;

    public LockFreeRingBuffer(int maxcapacity, ObjectFactory<E> factory) {
        mincapacity = factory == null ? MINBUFFER : maxcapacity;
        if (maxcapacity < mincapacity) maxcapacity = mincapacity;
        if (maxcapacity > 9999) maxcapacity = 9999;
        this.maxcapacity = ceilingNextPowerOfTwo(maxcapacity);
        this.factory = factory;
        array = new Object[mincapacity];
        capacity = mincapacity;
        mask = mincapacity - 1;
}

    public LockFreeRingBuffer(int maxcapacity) {
        this(maxcapacity, null);
    }

    public LockFreeRingBuffer(ObjectFactory<E> factory) {
        this(MINBUFFER, factory);
    }

    public boolean isEmpty() {
        return size.get() == 0; // do cache update
    }

    public int size() {
        return size.get(); // do cache update
    }

    public int getCapacity() {
        return maxcapacity;
    }

    public boolean isBacklogged() {
        // do not bother locking
        return tail - head > maxcapacity << 1;
    }

    private boolean testCapacity(int len) {
        if (len < capacity) return false;
        if (len == maxcapacity) return true; // full
        int newsize = capacity << 1;
        int newmask = newsize - 1;
        Object[] newarray = new Object[newsize];
        for (int index = mask; index >= 0; index--)
            // must fill everything because poll() may access newarray with old mask
            newarray[index | capacity] = newarray[index] = array[index];
        array = newarray;
        mask = newmask;
        capacity = newsize;
        return false;
    }

    private void reset(E e) {
        array[0] = e;
        head = 0;
        size.set(tail = 1);
    }

    private void wakeup() {
        if (taking) {
            synchronized (poll_lock) {
                idle = false;
                poll_lock.notify();
            }
        }
    }

    /**
     * use this for multiple producers
     */
    public boolean synced_offer(E e) {
        while (!offer_lock.compareAndSet(false, true)) // do cache update
            if (factory != null) return false;
        try {
            int s;
            if ((s = size.get()) > 0) { // do cache update
                if (testCapacity(s)) return false;
                array[tail++ & mask] = e;
                size.incrementAndGet(); // do cache update
                return true;
            }
            reset(e);
        } finally {
            offer_lock.set(false); // do cache update
        }
        wakeup();
        return true;
    }

    /**
     * this competes with any poll, so sync always
     */
    public boolean synced_offerFirst(E e) {
        while (!poll_lock.compareAndSet(false, true)) // do cache update
            if (factory != null) return false;
        try {
            int s;
            if ((s = size.get()) > 0) { // do cache update
                if (testCapacity(s)) return false;
                array[--head & mask] = e;
                size.incrementAndGet(); // do cache update
                return true;
            }
            reset(e);
        } finally {
            poll_lock.set(false); // do cache update
        }
        wakeup();
        return true;
    }

    /**
     * use this for multiple producers if success is unimportant
     * typical fast path for caches, but other uses cases exist
     */
    public boolean try_offer(E e) {
        if (!offer_lock.compareAndSet(false, true)) // do cache update
            return false;
        try {
            int s;
            if ((s = size.get()) > 0) { // do cache update
                if (testCapacity(s)) return false;
                array[tail++ & mask] = e;
                size.incrementAndGet(); // do cache update
                return true;
            }
            reset(e);
        } finally {
            offer_lock.set(false); // do cache update
        }
        wakeup();
        return true;
    }

    /**
     * use this for single producer
     */
    public boolean offer(E e) {
        int s;
        if ((s = size.get()) > 0) { // do cache update
            if (testCapacity(s)) return false;
            array[tail++ & mask] = e;
            size.incrementAndGet(); // do cache update
            return true;
        }
        reset(e);
        wakeup();
        return true;
    }

    public E peek() {
        if (size.get() == 0) // do cache update
            return null;
        return (E)array[head & mask];
    }

    /**
     * alternative to addAll() not requiring an iterator
     * 
     * @param c Collection to be filled
     */
    public void populate(Collection c) {
        // best effort
        int loops = size.get(); // do cache update
        for (int index = head; loops > 0; loops--) {
            E o = (E)array[index++ & mask];
            if (o != null) c.add(o); // there might have been a race
        }
    }

    public void drainTo(LockFreeRingBuffer<E> c) {
        E e;
        while ((e = poll()) != null)
            c.offer(e);
    }

    public void synced_drainTo(LockFreeRingBuffer<E> c) {
        E e;
        while ((e = synced_fast_poll()) != null)
            c.synced_offer(e);
    }

    public boolean contains(E o) {
        // best effort
        int loops = size.get(); // do cache update
        for (int index = head; loops > 0; loops--)
            if (o == array[index++ & mask])
                return true;
        return false;
    }

    /**
     * use this if the buffer frequently is empty (avoid lock)
     */
    public E synced_poll() {
        if (size.get() == 0) // do cache update
            return factory == null ? null : factory.newInstance();
        return synced_fast_poll();
    }

    /**
     * use this if the buffer usually is non-empty (cache scenario)
     */
    public E synced_fast_poll() {
        while (!poll_lock.compareAndSet(false, true)) { // do cache update
            if (factory != null) return factory.newInstance();
            if (size.get() == 0) // short circuit for races, lock not set
                return null;
        }
        try {
            return poll();
        } finally {
            poll_lock.set(false); // do cache update
        }
    }

    public int synced_pollArray(E[] buffer, int start, int len) {
        if (size.get() == 0) // do cache update
            return 0;
        while (!poll_lock.compareAndSet(false, true)) { // do cache update
            if (size.get() == 0 || len == 0) // short circuit for races, lock not set
                return 0;
        };
        try {
            int rv = Math.min(size.get(), len);
            for (int i = 0; i < rv; i++) {
                int h = head++ & mask;
                buffer[start++] = (E)array[h];
                array[h] = null;
                size.decrementAndGet(); // do cache update
            }
            return rv;
        } finally {
            poll_lock.set(false); // do cache update
        }
    }

    public void rotate() {
        E o = poll();
        if (o != null)
            offer(o);
    }

    public void synced_rotate(E o) { // single consumer only after successful peek()
        array[head++ & mask] = null;
        while (!offer_lock.compareAndSet(false, true)); // do cache update
        array[tail++ & mask] = o;
        offer_lock.set(false); // do cache update
    }

    public void pop() { // single consumer only after successful peek()
        array[head++ & mask] = null;
        size.decrementAndGet(); // do cache update
    }

    public E poll() {
        if (size.get() == 0) // do cache update
            return factory == null ? null : factory.newInstance();
        int h = head++ & mask;
        E rv = (E)array[h];
        array[h] = null;
        size.decrementAndGet(); // do cache update
        return rv;
    }

    private E poll1() {
        if (size.get() == 0) // do cache update
            return null;
        int h = head++ & mask;
        E rv = (E)array[h];
        array[h] = null;
        size.decrementAndGet(); // do cache update
        return rv;
    }

    public E take() {
        // do not let the evil OSes suspend us too long
        return take(50);
    }

    public E take(long timeOut) {
        idle = true;
        E rv;
        if ((rv = poll1()) != null) return rv;
        taking = true;
        synchronized (poll_lock) {
            try {
                if (idle) poll_lock.wait(timeOut);
            } catch (InterruptedException ie) {};
        }
        return poll1();
    }

    public void clear() {
        while (size() > 0) {
            while (!poll_lock.compareAndSet(false, true)); // do cache update
            poll1();
            poll_lock.set(false); // do cache update
        };
    }

    public void abort() {
        clear();
        synchronized (poll_lock) {
            idle = false;
            poll_lock.notify();
        }
    }
}