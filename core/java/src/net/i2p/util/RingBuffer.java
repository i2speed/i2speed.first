package net.i2p.util;

import java.util.Collection;
import java.util.Arrays;

/*
 * Free software
 */

/**
 * This implements part of the queue interface and is not thread-safe
 * minimum 128 items enforced
 * 
 * @param <E> the type of elements held in this collection
 */
public class RingBuffer<E>{

    private int ceilingNextPowerOfTwo(int x) {
        // From Hacker's Delight, Chapter 3, Harry S. Warren Jr.
        return 1 << (Integer.SIZE - Integer.numberOfLeadingZeros(x - 1));
    }

    final Object[] array;
    /* head pointing at first field valid */
    int head;
    /* tail pointing past last field valid */
    int tail;
    final int mask;
    final int ceiling;

    public RingBuffer(int estimatedCapacity) {
        if (estimatedCapacity <= 0) {
            throw new IllegalArgumentException();
        }
        if (estimatedCapacity <= 7) estimatedCapacity = 7;
        if (estimatedCapacity > 99999) estimatedCapacity = 99999;
        ceiling = ceilingNextPowerOfTwo(estimatedCapacity);
        array = new Object[ceiling];
        mask = ceiling - 1;
    }

    public RingBuffer(int estimatedCapacity, E initLRU) {
        this(estimatedCapacity);
        Arrays.fill(array, initLRU);
    }

    public E getLRU(E o) {
        E rv = (E)array[head & mask];
        if (o.equals(rv)) return rv;
        int limit = head + ceiling;
        for (int i = head + 1; limit - i > 0; i++) {
            rv = (E)array[i & mask];
            if (o.equals(rv)) {
                do {
                    array[i & mask] = array[i-- & mask];
                } while (i - head > 0);
                array[head & mask] = rv;
                return rv;
            }
        }
        array[--head & mask] = o;
        return o;
    }

    public boolean isEmpty() {
        return tail - head <= 0;
    }

    public int size() {
        return tail - head;
    }

    public int estimatedCapacity() {
        return ceiling;
    }

    /**
     */
    public boolean offer(E e) {
        if (tail - head > mask)
            return false; // full
        array[tail++ & mask] = e;
        return true;
    }

    public E peek() {
        if (tail - head <= 0)
            return null;
        return (E)array[head & mask];
    }

    public void rotate() {
        array[tail++ & mask] = array[head++ & mask];
    }

    /**
     * alternative to addAll() not requiring an iterator
     * 
     * @param c Collection to be filled
     */
    public void populate(Collection c) {
        for (int i = head; tail - i > 0; i++) {
            E o = (E)array[i & mask];
            if (o != null) // there might have been a concurrent poll()
                c.add(o);
        }
    }

    public boolean contains(E o) {
        for (int i = head; tail - i > 0; i++)
            if (o == array[i & mask])
                return true;
        return false;
    }

    /**
     */
    public E poll() {
        if (tail - head <= 0)
            return null;
        return (E)array[head++ & mask];
    }

    public void clear() {
        while (poll() != null)
        ;
    }
}