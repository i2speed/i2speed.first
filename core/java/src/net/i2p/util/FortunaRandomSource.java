package net.i2p.util;

/*
 * free (adj.): unencumbered; not under the control of others
 * Written by jrandom in 2003 and released into the public domain 
 * with no warranty of any kind, either expressed or implied.  
 * It probably won't make your computer catch on fire, or eat 
 * your children, but it might.  Use at your own risk.
 *
 */

import gnu.crypto.prng.AsyncFortunaStandalone;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.atomic.*;

import net.i2p.I2PAppContext;
import net.i2p.crypto.EntropyHarvester;
import net.i2p.data.DataHelper;

/**
 * Wrapper around GNU-Crypto's Fortuna PRNG.  This seeds from /dev/urandom and
 * ./prngseed.rnd on startup (if they exist), writing a new seed to ./prngseed.rnd
 * on an explicit call to saveSeed().
 *
 */
public class FortunaRandomSource extends RandomSource implements EntropyHarvester {
    private final AsyncFortunaStandalone _fortuna;
    private double _nextGaussian;
    private boolean _haveNextGaussian;

    /**
     *  May block up to 10 seconds or forever
     */
    public FortunaRandomSource(I2PAppContext context) {
        super(context);
        _fortuna = new AsyncFortunaStandalone(context);
        byte seed[] = new byte[1024];
        // may block for 10 seconds
        if (initSeed(seed)) {
            _fortuna.seed(seed);
        } else {
            // may block forever
            //SecureRandom sr = new SecureRandom();
            // SecureRandom already failed in initSeed(), so try Random
            Random sr = new Random();
            sr.nextBytes(seed);
            _fortuna.seed(seed);
        }
        _fortuna.startup();
        // kickstart it
        _fortuna.nextBytes(seed);
        _haveNextGaussian = false;
    }
    
    /**
     *  Note - methods may hang or NPE or throw IllegalStateExceptions after this
     *  @since 0.8.8
     */
    public void shutdown() {
        synchronized(_fortuna) {
            _fortuna.shutdown();
        }
    }

    @Override
    public void setSeed(byte buf[]) {
        synchronized(_fortuna) {
            _fortuna.addRandomBytes(buf);
        }
    }

    /**
     * According to the java docs (http://java.sun.com/j2se/1.4.1/docs/api/java/util/Random.html#nextInt(int))
     * nextInt(n) should return a number between 0 and n (including 0 and excluding n).  However, their pseudocode,
     * as well as sun's, kaffe's, and classpath's implementation INCLUDES NEGATIVE VALUES.
     * Ok, so we're going to have it return between 0 and n (including 0, excluding n), since 
     * thats what it has been used for.
     *
     */
    @Override
    public int nextInt(int n) {
        if (n <= 1) return 0;
        return signedNextInt(n);
    }
    
    @Override
    public int nextInt() { return fourBytes() & Integer.MAX_VALUE; }

    private static int INT_CACHE_SIZE = 32768;
    private byte[] _intCache = new byte[INT_CACHE_SIZE];
    private int _intCacheCount = INT_CACHE_SIZE;
    private AtomicBoolean intCacheLock = new AtomicBoolean();
    /**
     * Implementation from Sun's java.util.Random javadocs
     */
    private int signedNextInt(int n) {
        int numBytes;
        int less1 = n - 1;
        boolean pot;
        if ((pot = (n & less1) == 0)) {
            if (n <= 0x100)
                numBytes = 1;
            else if (n <= 0x10000)
                numBytes = 2;
            else if (n <= 0x1000000)
                numBytes = 3;
            else
                return fourBytes() & less1;
        } else {
            if (n <= 0x10)
                numBytes = 1;
            else if (n <= 0x1000)
                numBytes = 2;
            else if (n <= 0x100000)
                numBytes = 3;
            else
                return (fourBytes() & Integer.MAX_VALUE) % n;
        }
        
        while (!intCacheLock.compareAndSet(false, true));
        int tmp =_intCacheCount;
        if ((_intCacheCount += numBytes) > INT_CACHE_SIZE) {
            synchronized(_fortuna) {
                _fortuna.nextBytes(_intCache);
            }
            _intCacheCount = numBytes;
            tmp = 0;
        }
        int rv = (int)DataHelper.fromLong(_intCache, tmp, numBytes);
        intCacheLock.set(false);
        if (pot) return rv & less1;
        return (rv & Integer.MAX_VALUE) % n;
    }

    private static int FOUR_CACHE_SIZE = 32768;
    private byte[] _fourCache = new byte[FOUR_CACHE_SIZE];
    private int _fourCacheCount = FOUR_CACHE_SIZE;
    private AtomicBoolean fourCacheLock = new AtomicBoolean();

    private int fourBytes() {
        while (!fourCacheLock.compareAndSet(false, true));
        try {
            int tmp =_fourCacheCount;
            if ((_fourCacheCount += 4) > FOUR_CACHE_SIZE) {
                synchronized(_fortuna) {
                    _fortuna.nextBytes(_fourCache);
                }
                _fourCacheCount = 4;
                tmp = 0;
            }
            return DataHelper.fromInt4(_fourCache, tmp);
        } finally {fourCacheLock.set(false);}
    }

    private static long LONG_CUTOFF = (Integer.MAX_VALUE & 0xffffffffL) + 1;
    /**
     * Like the modified nextInt, nextLong(n) returns a random number from 0 through n,
     * including 0, excluding n.
     */
    @Override
    public long nextLong(long n) {
        if (n <= 1L<<32) {
            if (n > LONG_CUTOFF) {
                long rv = fourBytes() & 0xffffffffL;
                return rv < n ? rv : rv - Integer.MAX_VALUE;
            }
            if (n < LONG_CUTOFF) {
                int i = (int)n;
                if (i <= 1) return 0;
                return signedNextInt(i);
            }
            return nextInt();  // ultrarare: n == LONG_CUTOFF
        }
        return nextLong() % n;
    }
    
    @Override
    public long nextLong() { return signedNextLong() & Long.MAX_VALUE; }

    private static int LONG_CACHE_SIZE = 32768;
    private byte[] _longCache = new byte[LONG_CACHE_SIZE];
    private int _longCacheCount = LONG_CACHE_SIZE;
    private AtomicBoolean longCacheLock = new AtomicBoolean();
    /**
     * Implementation from Sun's java.util.Random javadocs
     */
    private long signedNextLong() {
        while (!longCacheLock.compareAndSet(false, true));
        try {
            int tmp =_longCacheCount;
            if ((_longCacheCount += 8) > LONG_CACHE_SIZE) {
                synchronized(_fortuna) {
                    _fortuna.nextBytes(_longCache);
                }
                tmp = 0;
                _longCacheCount = 8;
            }
            return DataHelper.fromLong8(_longCache, tmp);
        } finally {longCacheLock.set(false);}
    }

    private long _booleanCache;
    private int _booleanCacheCount = 64;
    private AtomicBoolean booleanCacheLock = new AtomicBoolean();
    @Override
    public boolean nextBoolean() { 
        while (!booleanCacheLock.compareAndSet(false, true));
        try {
            if (_booleanCacheCount == 64) {
                _booleanCacheCount = 0;
                _booleanCache = signedNextLong();
            }
            return ((int)(_booleanCache >>> _booleanCacheCount++) & 0x01) != 0;
        } finally {booleanCacheLock.set(false);}
    }

    @Override
    public void nextBytes(byte buf[]) { 
        nextBytes(buf, 0, buf.length);
        }
    private static int BYTE_CACHE_SIZE = 32768;
    private byte[] _byteCache = new byte[BYTE_CACHE_SIZE];
    private int _byteCacheCount = BYTE_CACHE_SIZE;
    private AtomicBoolean byteCacheLock = new AtomicBoolean();

    /**
     * Not part of java.util.SecureRandom, but added for efficiency, since Fortuna supports it.
     *
     * @since 0.8.12
     */
    @Override
    public void nextBytes(byte buf[], int offset, int length) {
        if (length >= BYTE_CACHE_SIZE >> 2) { // avoid fill-up below
            synchronized(_fortuna) {
                _fortuna.nextBytes(buf, offset, length);
            }
            return;
        }
        while (!byteCacheLock.compareAndSet(false, true));
        try {
            int tmp =_byteCacheCount;
            if ((_byteCacheCount += length) > BYTE_CACHE_SIZE) {
                synchronized(_fortuna) {
                    _fortuna.nextBytes(_byteCache);
                    _fortuna.nextBytes(buf, offset, length);
                }
                _byteCacheCount = 0;
                return;
            }
            System.arraycopy(_byteCache, tmp, buf, offset, length);
        } finally {byteCacheLock.set(false);}
    }

    /**
     * Not part of java.util.SecureRandom, but added for efficiency, since Fortuna supports it.
     *
     * @since 0.9.24
     */
    public byte nextByte() { 
        synchronized(_fortuna) {
            return _fortuna.nextByte();
        }
    }

    private static final long BITS53 = 1 << 53;
    /**
     * Implementation from sun's java.util.Random javadocs 
     */
    @Override
    public double nextDouble() { 
        return nextLong(BITS53) / (double)(BITS53);
    }

    private static final int BITS24 = 1 << 24;
    /**
     * Implementation from sun's java.util.Random javadocs 
     */
    @Override
    public float nextFloat() { 
        return signedNextInt(BITS24) / (float)(BITS24);
    }

    /**
     * Implementation from sun's java.util.Random javadocs 
     */
    @Override
    public double nextGaussian() { 
        synchronized (this) {
            if (_haveNextGaussian) {
                _haveNextGaussian = false;
                return _nextGaussian;
            }
            double v1, v2, s;
            do { 
                v1 = 2 * nextDouble() - 1;   // between -1.0 and 1.0
                v2 = 2 * nextDouble() - 1;   // between -1.0 and 1.0
                s = v1 * v1 + v2 * v2;
            } while (s >= 1 || s == 0);
            double multiplier = Math.sqrt(-2 * Math.log(s)/s);
            _nextGaussian = v2 * multiplier;
            _haveNextGaussian = true;
            return v1 * multiplier;
        }
    }

    /**
     * Pull the next numBits of random data off the fortuna instance (returning 0
     * through 2^numBits-1
     *
     * Caller must synchronize!
     */
    protected long nextBits(int numBits) {
        long rv = 0;
        int bytes = (numBits + 7) / 8;
        for (int i = 0; i < bytes; i++)
            rv += ((_fortuna.nextByte() & 0xFF) << i*8);
        //rv >>>= (64-numBits);
        if (rv < 0)
            rv = 0 - rv;
        int off = 8*bytes - numBits;
        rv >>>= off;
        return rv;
    }
    
    @Override
    public EntropyHarvester harvester() { return this; }
 
    /** reseed the fortuna */
    @Override
    public void feedEntropy(String source, long data, int bitoffset, int bits) {
        synchronized(_fortuna) {
            _fortuna.addRandomByte((byte)(data & 0xFF));
        }
    }
 
    /** reseed the fortuna */
    @Override
    public void feedEntropy(String source, byte[] data, int offset, int len) {
        try {
            synchronized(_fortuna) {
                _fortuna.addRandomBytes(data, offset, len);
            }
        } catch (RuntimeException e) {
            // AIOOBE seen, root cause unknown, ticket #1576
            Log log = _context.logManager().getLog(FortunaRandomSource.class);
            log.warn("feedEntropy()", e);
        }
    }
    
    /**
     *  Outputs to stdout for dieharder:
     *  <code>
     *  java -cp build/i2p.jar net.i2p.util.FortunaRandomSource | dieharder -a -g 200
     *  </code>
     */
    public static void main(String args[]) {
        try {
            java.util.Properties props = new java.util.Properties();
            props.setProperty("prng.buffers", "12");
            I2PAppContext ctx = new I2PAppContext(props);
            RandomSource rand = ctx.random();
            byte[] buf = new byte[65536];
            while (true) {
                rand.nextBytes(buf);
                System.out.write(buf);
            }
        } catch (IOException e) { e.printStackTrace(); }
    }
}
